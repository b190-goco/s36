// contains functions and business logic of our express js application
// all operations it can do will be placed in this file 
// allows us to use the contents of 'task.js' file in the models folder

const Task = require("../models/task.js");

// defines the function to be used in the taskRoutes.js
module.exports.getAllTasks = () => {
	return Task.find({ }).then(result => {
		return result
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	});
	return newTask.save().then((task,error) => {
		if (error) {
			console.log(error);
			return false
		}
		else {
			return task;
		}
	})
}

/*module.exports.deleteTask = (taskId) => {
	console.log(taskId);
	return Task.findByIdAndRemove(requestParams).then((result,error) => {
		if(error) {

			console.log(error);
			return false
		}
		else {
			return result
		}

	})
}*/

module.exports.deleteTask = (taskId) => {
	// findByIdAndRemove - is a mongoose method that searches for the documents using the _id properties; after finding a document, the job of this method is to remove the document
	return Task.findByIdAndRemove(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			return result
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			result.name = newContent.name;
			return result.save().then((updatedTask, error)=>{
				if (error){
					console.log(error);
					return false
				}else{
					return updatedTask;
				};
			});
		};
	});
};

module.exports.findTask = (taskId) => {
	return Task.findById(taskId).then((result,error) => {
		if (error) {
			console.log(error);
			return false
		}
		else {
			return result
		}
	})
};

module.exports.updateStatus = (taskId, taskStatus) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			result.status = taskStatus;
			return result.save().then((updatedStatus, error)=>{
				if (error){
					console.log(error);
					return false
				}else{
					return updatedStatus;
				};
			});
		};
	});
};