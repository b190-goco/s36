// contains all endpoints for our application

const express = require("express");

// allows to create/access http method middlewares that makes it easier to create routing system
const router = express.Router();

const taskController = require("../controllers/taskController.js");

router.get("/", (req,res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// miniactivity

// create a "/" route that will process the request and send a response based on the createTask function inside the "taskController.js"
router.post("/", (req,res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
	//console.log(req.body)
})

router.delete("/:id", (req,res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
	//console.log(req.params.id)
})

router.put("/:id", (req,res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// 1. Create a route for getting a specific task.
// 2. Create a controller function for retrieving a specific task.
// 3. Return the result back to the client/Postman.
// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
router.get("/:id", (req,res) => {
	taskController.findTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// 5. Create a route for changing the status of a task to "complete".
// 6. Create a controller function for changing the status of a task to "complete".
// 7. Return the result back to the client/Postman.
// 8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
router.put("/:id/:status", (req,res) => {
	taskController.updateStatus(req.params.id,req.params.status).then(resultFromController => res.send(resultFromController));
})
// 9. Create a git repository named S36.
// 10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 11. Add the link in Boodle.

module.exports = router;